/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <modal_json.h>
#include "config_file.h"

#ifdef APQ8096

#define FILE_HEADER "\
/**\n\
 * voxl-mavlink-server Configuration File\n\
 *\n\
 * primary_static_gcs_ip & secondary_static_gcs_ip\n\
 *    These configure voxl-mavlink-server to automatically try to connect to\n\
 *    up to two known static GCS units. Set to empty or NULL if you don't want\n\
 *    to use this and you want the GCS to initialize the connection instead.\n\
 *    Note the default IP for the primary link is 192.168.8.10 which is the\n\
 *    first IP that VOXL DHCP serves when connecting in wifi softap mode.\n\
 *\n\
 *    DON'T set either of these ip addresses to localhost, these are meant\n\
 *    for external ground control stations. For mavsdk and mavros communication\n\
 *    locally, set the 'en_localhost_mavlink_udp' flag in voxl-vision-hub's\n\
 *    /etc/modalai/voxl-vision-hub.conf file instead.\n\
 *\n\
 *\n\
 * autopilot_uart_bus        - primary uart bus, default 5 for voxl flight \n\
 * autopilot_uart_baudrate   - default 921600\n\
 * autopilot_uart_backup_bus - backup port auto-selected if primary is not connected\n\
 *                             set to 12 for VOXL J11\n\
 * autopilot_mission_delay_start - default -1 (off), >0 means to delay mission start for X seconds\n\
 * autopilot_mission_delay_sound - play esc chime tone/behavior, true=on false=off\n\
 * autopilot_mission_notif_dur   - visual notification using motor spin, duration in seconds\n\
 * en_external_fc_timesync   - enable responding to timesync messages\n\
 *                                   (enabled by default)\n\
 * udp_mtu - maximum transfer unit for UDP packets back to GCS. voxl-mavlink-server\n\
 *           will bundle up backets for the GCS into a single UDP packet with \n\
 *           a maxium size of this. This might help with some radios.\n\
 *           This feature is off by default to let the network stack handle aggregation.\n\
 *           Set to 0 to disable this feature and send one UDP packet per msg.\n\
 *           Set to something like 500 to bundle a handful of packets together.\n\
 *\n\
 */\n"

#else

#define FILE_HEADER "\
/**\n\
 * voxl-mavlink-server Configuration File\n\
 *\n\
 * primary_static_gcs_ip & secondary_static_gcs_ip\n\
 *    These configure voxl-mavlink-server to automatically try to connect to\n\
 *    up to two known static GCS units. Set to empty or NULL if you don't want\n\
 *    to use this and you want the GCS to initialize the connection instead.\n\
 *    Note the default IP for the primary link is 192.168.8.10 which is the\n\
 *    first IP that VOXL DHCP serves when connecting in wifi softap mode.\n\
 *\n\
 *    DON'T set either of these ip addresses to localhost, these are meant\n\
 *    for external ground control stations. For mavsdk and mavros communication\n\
 *    locally, set the 'en_localhost_mavlink_udp' flag in voxl-vision-hub's\n\
 *    /etc/modalai/voxl-vision-hub.conf file instead.\n\
 *\n\
 *\n\
 * Settings for running voxl-px4 on SLPI:\n\
 * onboard_port_to_autopilot   - UDP port to send high-rate onboard data to SLPI\n\
 * onboard_port_from_autopilot - UDP port to receive high-rate onboard data from SLPI\n\
 * gcs_port_to_autopilot       - UDP port to send normal-rate gcs data to SLPI\n\
 * gcs_port_from_autopilot     - UDP port to receive normal-rate gcs data from SLPI\n\
 *\n\
 * Settings for running an external autopilot connected via UART:\n\
 * en_external_uart_ap       - set to true to enable an external flight controller\n\
 * autopilot_uart_bus        - uart bus, default 1 for VOXL2 \n\
 * autopilot_uart_baudrate   - default 921600\n\
 * autopilot_mission_delay_start - default -1 (off), >0 means to delay mission start for X seconds\n\
 * autopilot_mission_delay_sound - play esc chime tone/behavior, true=on false=off\n\
 * autopilot_mission_notif_dur   - visual notification using motor spin, duration in seconds\n\
 * en_external_fc_timesync   - enable responding to timesync messages\n\
 *                                   (enabled by default)\n\
 * en_external_ap_heartbeat  - enable automatic sending of heartbeat\n\
 * gcs_timeout_s - time without heartbeat to consider GCS disconnected\n\
 *\n\
 * udp_mtu - maximum transfer unit for UDP packets back to GCS. voxl-mavlink-server\n\
 *           will bundle up backets for the GCS into a single UDP packet with \n\
 *           a maxium size of this. This saves network traffic drastically.\n\
 *           Set to 0 to disable this feature and send one UDP packet per msg.\n\
 *\n\
 *\n\
 * External FC field is for QRB5165 only. Set to true to enable UART\n\
 * communication to an external flight controller, otherwise a UDP interface\n\
 * will be started to talk to voxl-px4 on localhost which is the default behavior.\n\
 * Select UART port 1 to go through the legacy B2B connector, that's the port exposed by the\n\
 * M0125 and M0141 accessory boards. Use port 12 to go through the ESC port (J18).\n\
 *\n\
 */\n"
#endif

#ifdef APQ8096
	#define DEFAULT_UART_BUS 5
	#define DEFAULT_UART_BACKUP_BUS 12 // Port J11 on VOXL1
	int autopilot_uart_backup_bus;
#else
	#define DEFAULT_UART_BUS 1
	int onboard_port_to_autopilot;
	int onboard_port_from_autopilot;
	int gcs_port_to_autopilot;
	int gcs_port_from_autopilot;
#endif

// common things
char primary_static_gcs_ip[64];
char secondary_static_gcs_ip[64];
int autopilot_uart_bus;
int autopilot_uart_baudrate;
int autopilot_mission_delay_start;
int autopilot_mission_delay_sound;
double autopilot_mission_notif_dur;
int en_external_ap_timesync;
int en_external_ap_heartbeat;
int udp_mtu;
double gcs_timeout_s;
// default the variable to 1 since APQ8096 this should be 1 btu is not present
// in the config file. QRB5165 the config defaults to 0 but might be set to 1
int en_external_uart_ap = 1;


int config_file_print(void)
{
	printf("=================================================================");
	printf("\n");
	printf("Parameters as loaded from config file:\n");
	printf("primary_static_gcs_ip:        %s\n", primary_static_gcs_ip);
	printf("secondary_static_gcs_ip:      %s\n", secondary_static_gcs_ip);
	#ifdef APQ8096
	printf("autopilot_uart_backup_bus:    %d\n", autopilot_uart_backup_bus);
	#else
	printf("onboard_port_to_autopilot:    %d\n", onboard_port_to_autopilot);
	printf("onboard_port_from_autopilot:  %d\n", onboard_port_from_autopilot);
	printf("gcs_port_to_autopilot:        %d\n", gcs_port_to_autopilot);
	printf("gcs_port_from_autopilot:      %d\n", gcs_port_from_autopilot);
	printf("en_external_uart_ap:          %d\n", en_external_uart_ap);
	#endif
	printf("autopilot_uart_bus:           %d\n", autopilot_uart_bus);
	printf("autopilot_uart_baudrate:      %d\n", autopilot_uart_baudrate);
	printf("autopilot_mission_delay_start:      %d\n", autopilot_mission_delay_start);
	printf("autopilot_mission_delay_sound:      %d\n", autopilot_mission_delay_sound);
	printf("autopilot_mission_notif_dur:      %0.2f\n", autopilot_mission_notif_dur);
	printf("udp_mtu:                      %d\n", udp_mtu);
	printf("gcs_timeout_s                 %0.2f\n", gcs_timeout_s);
	printf("en_external_ap_timesync:      %d\n", en_external_ap_timesync);
	printf("en_external_ap_heartbeat:     %d\n", en_external_ap_heartbeat);
	printf("=================================================================");
	printf("\n");
	return 0;
}


int config_file_load(void)
{
	// check if the file exists and make a new one if not
	int ret = json_make_empty_file_with_header_if_missing(CONF_FILE, FILE_HEADER);
	if(ret < 0) return -1;
	else if(ret>0) fprintf(stderr, "Created new json file: %s\n", CONF_FILE);

	// read the data in
	cJSON* parent = json_read_file(CONF_FILE);
	if(parent==NULL) return -1;


	// common things
	json_fetch_string_with_default( parent, "primary_static_gcs_ip", primary_static_gcs_ip, 63, "192.168.8.10");
	json_fetch_string_with_default( parent, "secondary_static_gcs_ip", secondary_static_gcs_ip, 63, "192.168.8.11");

	#ifdef APQ8096
	json_fetch_int_with_default(	parent, "autopilot_uart_backup_bus", &autopilot_uart_backup_bus, DEFAULT_UART_BACKUP_BUS);
	if(autopilot_uart_backup_bus==autopilot_uart_bus){
		fprintf(stderr, "ERROR, backup uart bus can't be the same as the primary\n");
		return -1;
	}

	#else // QRB5165 only
	json_fetch_int_with_default(	parent, "onboard_port_to_autopilot", &onboard_port_to_autopilot, 14556);
	json_fetch_int_with_default(	parent, "onboard_port_from_autopilot", &onboard_port_from_autopilot, 14557);
	json_fetch_int_with_default(	parent, "gcs_port_to_autopilot", &gcs_port_to_autopilot, 14558);
	json_fetch_int_with_default(	parent, "gcs_port_from_autopilot", &gcs_port_from_autopilot, 14559);
	json_fetch_bool_with_default(	parent, "en_external_uart_ap", &en_external_uart_ap, 0);
	#endif

	// common things
	json_fetch_int_with_default(	parent, "autopilot_uart_bus", &autopilot_uart_bus, DEFAULT_UART_BUS);
	json_fetch_int_with_default(	parent, "autopilot_uart_baudrate", &autopilot_uart_baudrate, 921600);
	json_fetch_int_with_default(	parent, "autopilot_mission_delay_start", &autopilot_mission_delay_start, -1);
	json_fetch_bool_with_default(	parent, "autopilot_mission_delay_sound", &autopilot_mission_delay_sound, 0);
	json_fetch_double_with_default(	parent, "autopilot_mission_notif_dur", & autopilot_mission_notif_dur, 0.1); // starlingv1/v2 motors 0.25

	json_fetch_int_with_default(	parent, "en_external_ap_timesync", &en_external_ap_timesync, 1);
	json_fetch_int_with_default(	parent, "en_external_ap_heartbeat", &en_external_ap_heartbeat, 1);
	json_fetch_int_with_default(	parent, "udp_mtu", &udp_mtu, 0);
	json_fetch_double_with_default(	parent, "gcs_timeout_s", & gcs_timeout_s, 4.5);


	// check if we got any errors in that process
	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse data in %s\n", CONF_FILE);
		cJSON_Delete(parent);
		return -1;
	}

	// write modified data to disk if neccessary
	if(json_get_modified_flag()){
		// printf("The JSON config file data was modified during parsing, saving the changes to disk\n");
		json_write_to_file_with_header(CONF_FILE, parent, FILE_HEADER);
	}
	cJSON_Delete(parent);
	return 0;
}
