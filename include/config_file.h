/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef CONFIG_FILE_H
#define CONFIG_FILE_H


#define CONF_FILE "/etc/modalai/voxl-mavlink-server.conf"


#ifdef APQ8096
	extern int autopilot_uart_backup_bus;
#else
	extern int onboard_port_to_autopilot;
	extern int onboard_port_from_autopilot;
	extern int gcs_port_to_autopilot;
	extern int gcs_port_from_autopilot;
#endif

// common things
extern char primary_static_gcs_ip[64];
extern char secondary_static_gcs_ip[64];
extern int autopilot_uart_bus;
extern int autopilot_uart_baudrate;
// customized mission feature
extern int autopilot_mission_delay_start;
extern int autopilot_mission_delay_sound;
extern double autopilot_mission_notif_dur;
extern int en_external_ap_timesync;
extern int en_external_ap_heartbeat;
extern int udp_mtu;
extern double gcs_timeout_s;
// always true on APQ8096
extern int en_external_uart_ap;


// load only our own config file without printing the contents
int config_file_load(void);

// prints the current configuration values to the screen.
int config_file_print(void);

#endif // end #define CONFIG_FILE_H
